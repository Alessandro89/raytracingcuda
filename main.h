//#ifndef __MAIN_H
//#define __MAIN_H

#define ROWS 1200//512
#define COLS 1200//512
#define TxL 16 //16
#define TxH 16  //16

#define MAXLIGHT 2
#define MAXRECTANGLE 3
#define MAXSPHERE 3
//usiamo array
__constant__ light cst_lights[MAXLIGHT];
__constant__ sphere cst_spheres[MAXSPHERE];
__constant__ rectangular cst_rectangulars[MAXRECTANGLE];
__constant__ viewpoint cst_view[1];
__constant__ color cst_backgroundColor;

//#endif
