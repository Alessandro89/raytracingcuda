#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include <stdint.h>
#include <time.h>
#include <malloc.h>
#include "primitives.h"
#include "raytracing2.h"
#include "common.h"
#include "raytracingcuda.h"
#include "main.h"
#include <windows.h>
#include <cuda_profiler_api.h>
#include <assert.h>


#define OUT_FILENAME "outHost.ppm"
#define OUT_FILENAMECUDA "outCuda.ppm"


static void write_to_ppm(FILE *outfile, uint8_t *pixels,
                         int width, int height)
{
    fprintf(outfile, "P6\n%d %d\n%d\n", width, height, 255);
    fwrite(pixels, 1, height * width * 3, outfile);
	fflush(outfile);
}

static void testing(unsigned char *pixels, unsigned char *pixelscu, long nBytesPixel) {
	printf("testing: ");
	bool correct = true;
	unsigned char *pixeltmp = pixels;
	unsigned char *pixeltmpCu = pixelscu;

	for (long i = 0; i < nBytesPixel; i++) {
		int aPixel = *pixeltmp;
		int aPixelcu = *pixeltmpCu;

		if (aPixelcu > aPixel || aPixelcu < aPixel) {
			printf("pixel errato! %d:\n", i);
			printf("%d;", aPixelcu);
			printf("%d;\n", aPixel);
			correct = false;
		}
		pixeltmp++;
		pixeltmpCu++;
	}

	if (!correct) {
		printf("\n");
		assert(correct && "The algorithm is not correct!");
	
	}
	

	else {
		printf("is correct!\n");
	}
}

int main()
{
	//per far si che l'algoritmo sia corretto
	assert(ROWS == COLS && "Errore, rows e cols devono essere uguali..");
	unsigned char *pixels;
	unsigned char *pixelscu;
	FILE *outfile = NULL;

	///-------------------------------------------DATA------------------------------------------------------------------------////
	color background = { 0, 0, 0 };

	const int sizeLights = 2;
	const int sizeSpheres = 3;
	const int sizeRectangles = 3;
	light light1;
	light1.light_color[0] = 0.7;
	light1.light_color[1] = 0.5;
	light1.light_color[2] = 0.5;
	light1.position[0] = 5;
	light1.position[1] = 5;
	light1.position[2] = 20;
	light1.intensity = 200.0;

	light light2;
	light2.light_color[0] = 0.8;
	light2.light_color[1] = 0.8;
	light2.light_color[2] = 0.8;
	light2.position[0] = 10;
	light2.position[1] = 10;
	light2.position[2] = 20;
	light2.intensity = 200.0;

	sphere sphere1;
	sphere1.center[0] = 5;
	sphere1.center[1] = 0;
	sphere1.center[2] = 5;
	sphere1.radius = 3;
	sphere1.sphere_fill.fill_color[0] = 0.8;
	sphere1.sphere_fill.fill_color[1] = 0.8; 
	sphere1.sphere_fill.fill_color[2] = 0.8; 
	sphere1.sphere_fill.Kd = 0.8;
	sphere1.sphere_fill.Ks = 0.8;
	sphere1.sphere_fill.T = 0.0;
	sphere1.sphere_fill.R = 0.6;
	sphere1.sphere_fill.index_of_refraction= 0.0;
	sphere1.sphere_fill.phong_power = 30.0;

	sphere sphere2;
	sphere2.center[0] = 20;
	sphere2.center[1] = 15;
	sphere2.center[2] = 15;
	sphere2.radius = 3;
	sphere2.sphere_fill.fill_color[0] = 0.8;
	sphere2.sphere_fill.fill_color[1] = 0.6;
	sphere2.sphere_fill.fill_color[2] = 0.4;
	sphere2.sphere_fill.Kd = 0.0;
	sphere2.sphere_fill.Ks = 1.0;
	sphere2.sphere_fill.T = 1.0;
	sphere2.sphere_fill.R = 1.0;
	sphere2.sphere_fill.index_of_refraction = 1.5;
	sphere2.sphere_fill.phong_power = 30.0;

	sphere sphere3;
	sphere3.center[0] = 5;
	sphere3.center[1] = 10;
	sphere3.center[2] = 5;
	sphere3.radius = 3;
	sphere3.sphere_fill.fill_color[0] = 0.4;
	sphere3.sphere_fill.fill_color[1] = 0.8;
	sphere3.sphere_fill.fill_color[2] = 0.6;
	sphere3.sphere_fill.Kd = 0.8;
	sphere3.sphere_fill.Ks = 0.1;
	sphere3.sphere_fill.T = 0.0;
	sphere3.sphere_fill.R = 0.1;
	sphere3.sphere_fill.index_of_refraction = 0.0;
	sphere3.sphere_fill.phong_power = 30.0;

	viewpoint view;
	view.vrp[0] = 40.0;
	view.vrp[1] = 40.0;
	view.vrp[2] = 40.0;
	view.vpn[0] = -1.0;
	view.vpn[1] = -1.0;
	view.vpn[2] = -1.0;
	view.vup[0] = 0.0;
	view.vup[1] = 0.0;
	view.vup[2] = 1.0;

	rectangular rectangular1;
	rectangular1.vertices[0][0] = 0.0;
	rectangular1.vertices[0][1] = 0.0;
	rectangular1.vertices[0][2] = 0.0;
	rectangular1.vertices[1][0] = 0.0;
	rectangular1.vertices[1][1] = 0.0;
	rectangular1.vertices[1][2] = 20.0;
	rectangular1.vertices[2][0] = 20.0;
	rectangular1.vertices[2][1] = 0.0;
	rectangular1.vertices[2][2] = 20.0;
	rectangular1.vertices[3][0] = 20.0;
	rectangular1.vertices[3][1] = 0.0;
	rectangular1.vertices[3][2] = 0.0;
	rectangular1.normal[0] = 0.0;
	rectangular1.normal[1] = 1.0;
	rectangular1.normal[2] = 0.0;
	rectangular1.rectangular_fill.fill_color[0] = 0.6;
	rectangular1.rectangular_fill.fill_color[1] = 0.6;
	rectangular1.rectangular_fill.fill_color[2] = 0.6;
	rectangular1.rectangular_fill.Kd = 0.8;
	rectangular1.rectangular_fill.Ks = 0.0;
	rectangular1.rectangular_fill.T = 0.0;
	rectangular1.rectangular_fill.R = 0.5;
	rectangular1.rectangular_fill.index_of_refraction = 0.0;
	rectangular1.rectangular_fill.phong_power = 5.0;

	rectangular rectangular2;
	rectangular2.vertices[0][0] = 0.0;
	rectangular2.vertices[0][1] = 0.0;
	rectangular2.vertices[0][2] = 0.0;
	rectangular2.vertices[1][0] = 20.0;
	rectangular2.vertices[1][1] = 0.0;
	rectangular2.vertices[1][2] = 0.0;
	rectangular2.vertices[2][0] = 20.0;
	rectangular2.vertices[2][1] = 20.0;
	rectangular2.vertices[2][2] = 0.0;
	rectangular2.vertices[3][0] = 0.0;
	rectangular2.vertices[3][1] = 20.0;
	rectangular2.vertices[3][2] = 0.0;
	rectangular2.normal[0] = 0.0;
	rectangular2.normal[1] = 0.0;
	rectangular2.normal[2] = 1.0;
	rectangular2.rectangular_fill.fill_color[0] = 0.6;
	rectangular2.rectangular_fill.fill_color[1] = 0.1;
	rectangular2.rectangular_fill.fill_color[2] = 0.1;
	rectangular2.rectangular_fill.Kd = 0.8;
	rectangular2.rectangular_fill.Ks = 0.0;
	rectangular2.rectangular_fill.T = 0.0;
	rectangular2.rectangular_fill.R = 0.5;
	rectangular2.rectangular_fill.index_of_refraction = 0.0;
	rectangular2.rectangular_fill.phong_power = 5.0;
	
	rectangular rectangular3;
	rectangular3.vertices[0][0] = 0.0;
	rectangular3.vertices[0][1] = 0.0;
	rectangular3.vertices[0][2] = 0.0;
	rectangular3.vertices[1][0] = 0.0;
	rectangular3.vertices[1][1] = 20.0;
	rectangular3.vertices[1][2] = 0.0;
	rectangular3.vertices[2][0] = 0.0;
	rectangular3.vertices[2][1] = 20.0;
	rectangular3.vertices[2][2] = 20.0;
	rectangular3.vertices[3][0] = 0.0;
	rectangular3.vertices[3][1] = 0.0;
	rectangular3.vertices[3][2] = 20.0;
	rectangular3.normal[0] = 1.0;
	rectangular3.normal[1] = 0.0;
	rectangular3.normal[2] = 0.0;
	rectangular3.rectangular_fill.fill_color[0] = 0.1;
	rectangular3.rectangular_fill.fill_color[1] = 0.1;
	rectangular3.rectangular_fill.fill_color[2] = 0.6;
	rectangular3.rectangular_fill.Kd = 0.8;
	rectangular3.rectangular_fill.Ks = 0.0;
	rectangular3.rectangular_fill.T = 0.0;
	rectangular3.rectangular_fill.R = 0.5;
	rectangular3.rectangular_fill.index_of_refraction = 0.0;
	rectangular3.rectangular_fill.phong_power = 5.0;
	
    /* allocate by the given resolution */
	int elements = ROWS * COLS * 3;
	long nBytesPixel = sizeof(unsigned char) * elements;
    pixels = (unsigned char*) malloc(nBytesPixel);
	pixelscu = (unsigned char*) malloc(nBytesPixel);
	
	//create arrays of objects
	rectangular listRectangles[sizeRectangles] = {rectangular1,rectangular2,rectangular3};
	sphere listSphere[sizeSpheres] = {sphere1,sphere2,sphere3};
	light listLight[sizeLights] = {light1,light2};
	///-------------------------------------------HOST------------------------------------------------------------------------////

	DWORD startTime = GetTickCount();
	raytracing2(pixels, background,
		listRectangles, listSphere,
		listLight, view, ROWS, COLS, sizeRectangles, sizeSpheres, sizeLights);
	DWORD endTime = GetTickCount();
	printf("milliseconds host: %lu \n", endTime - startTime);
	
	//write
	outfile = fopen(OUT_FILENAME, "wb");
	write_to_ppm(outfile, pixels, ROWS, COLS);
	fclose(outfile);
	
	///-------------------------------------------CUDA------------------------------------------------------------------------////

	dim3 block(TxL, TxH);
	dim3 grid(ROWS / TxH + 1, COLS / TxL + 1);
	

	callraytracingcuda(grid, block, nBytesPixel, background, listRectangles, listSphere, listLight, view, sizeRectangles, sizeSpheres, sizeLights, pixelscu);

	//CHECK(cudaProfilerStop());

	//write
	outfile = fopen(OUT_FILENAMECUDA, "wb");
	write_to_ppm(outfile, pixelscu, ROWS, COLS);
	fclose(outfile);

	///-------------------------------------------TESTING------------------------------------------------------------------------////
	/*
	Testing test:
	unsigned char one[] = { 10, 100, 200, 240, 249 };
	unsigned char second[] = { 10, 100, 201, 240, 250 };
	unsigned char *testone= one;
	unsigned char *testsecond = second;
	testing(testone,testsecond,5);
	*/
	testing(pixels, pixelscu, nBytesPixel);

	///------------------------------------------FREE MEMORY-----------------------------------------------------------------////
    free(pixels);
	free(pixelscu);
	// reset device
	cudaDeviceReset();
    printf("Done!\n");
    return 0;
}
