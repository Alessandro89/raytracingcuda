#ifndef __RAYTRACING2_H
#define __RAYTRACING2_H

#include <stdint.h>

void raytracing2(uint8_t *pixels, color background_color,
	rectangular listRectangles[], sphere listSphere[],
	light listLight[], viewpoint view, int height, int width, int sizeRectangles, int sizeSpheres, int sizeLights);

#endif
