#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include "math.h"
#include "primitives.h"
#include "raytracingcuda.h"
#include "idx_stack.h"
#include "main.h"
#include <assert.h>
#include "device_atomic_functions.h"

#define MAX_REFLECTION_BOUNCES	3 //3
#define MAX_DISTANCE 1000000000000.0
#define MIN_DISTANCE 0.00001
#define SAMPLES 4

#define SQUARE(x) (x * x)
#define MAX(a, b) (a > b ? a : b)

__device__ void normalize(double *v)
{
	double d = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	//assert(d != 0.0 && "Error calculating normal");

	v[0] /= d;
	v[1] /= d;
	v[2] /= d;
}

__device__ double length(const double *v)
{
	return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

__device__ void add_vector(const double *a, const double *b, double *out)
{

	for (int i = 0; i < 3; i++)
		out[i] = a[i] + b[i];
}

__device__ void subtract_vector(const double *a, const double *b, double *out)
{

	for (int i = 0; i < 3; i++)
		out[i] = a[i] - b[i];
}

__device__ void multiply_vectors(const double *a, const double *b, double *out)
{

	for (int i = 0; i < 3; i++)
		out[i] = a[i] * b[i];
}

__device__ void multiply_vector(const double *a, double b, double *out)
{

	for (int i = 0; i < 3; i++)
		out[i] = a[i] * b;
}

__device__ void cross_product(const double *v1, const double *v2, double *out)
{
	out[0] = v1[1] * v2[2] - v1[2] * v2[1];
	out[1] = v1[2] * v2[0] - v1[0] * v2[2];
	out[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

__device__ double dot_product(const double *v1, const double *v2)
{
	double dp = 0.0;

	for (int i = 0; i < 3; i++)
		dp += v1[i] * v2[i];
	return dp;
}

__device__ void scalar_triple_product(const double *u, const double *v, const double *w,
	double *out)
{
	cross_product(v, w, out);
	multiply_vectors(u, out, out);
}

__device__ double scalar_triple(const double *u, const double *v, const double *w)
{
	double tmp[3];
	cross_product(w, u, tmp);
	return dot_product(v, tmp);
}

__device__ void calculateBasisVectors(point3 u, point3 v, point3 w,
	const viewpoint *view)
{
	/* w  */
	COPY_POINT3(w, view->vpn);
	normalize(w);

	/* u = (t x w) / (|t x w|) */
	cross_product(w, view->vup, u);
	normalize(u);

	/* v = w x u */
	cross_product(u, w, v);

	normalize(v);
}

__device__ void rayConstruction(point3 d, const point3 u, const point3 v,
	const point3 w, unsigned int i, unsigned int j,
	unsigned int width,
	unsigned int height)
{
	
	//double xmin = -0.0175;
	//double ymin = -0.0175;
	//double xmax = 0.0175;
	//double ymax = 0.0175;
	double min = -0.0175;
	double max = 0.0175;
	double w_s = 0.05;

	point3 u_tmp, v_tmp, w_tmp, s;

	//double w_s = focal;
	//double u_s = min + ((max - min) * (double)i / (width - 1));
	//double v_s = max + ((min - max) * (double)j / (height - 1));

	/* s = e + u_s * u + v_s * v + w_s * w */
	multiply_vector(u, min + ((max - min) * (double)i / (width - 1)), u_tmp);
	multiply_vector(v, max + ((min - max) * (double)j / (height - 1)), v_tmp);
	multiply_vector(w, w_s, w_tmp);
	add_vector(cst_view[0].vrp, u_tmp, s);
	add_vector(s, v_tmp, s);
	add_vector(s, w_tmp, s);

	/* p(t) = e + td = e + t(s - e) */
	subtract_vector(s, cst_view[0].vrp, d);
	normalize(d);
}

__device__ void protect_color_overflow(color c)
{
	for (int i = 0; i < 3; i++)
		if (c[i] > 1.0) c[i] = 1.0;
}

__device__ int raySphereIntersection(const point3 ray_e,
	const point3 ray_d,
	const sphere *sph,
	intersection *ip, double *t1)
{
	point3 l;
	subtract_vector(sph->center, ray_e, l);
	double s = dot_product(l, ray_d);
	double l2 = dot_product(l, l);
	double r2 = sph->radius * sph->radius;

	if (s < 0 && l2 > r2)
		return 0;
	double m2 = l2 - s * s;
	if (m2 > r2)
		return 0;
	double q = sqrt(r2 - m2);
	*t1 = (l2 > r2) ? (s - q) : (s + q);
	/* p = e + t1 * d */
	multiply_vector(ray_d, *t1, ip->point);
	add_vector(ray_e, ip->point, ip->point);

	subtract_vector(ip->point, sph->center, ip->normal);
	normalize(ip->normal);
	if (dot_product(ip->normal, ray_d) > 0.0)
		multiply_vector(ip->normal, -1, ip->normal);
	return 1;
}

__device__ int rayRectangularIntersection(const point3 ray_e,
	const point3 ray_d,
	rectangular *rec,
	intersection *ip, double *t1)
{
	point3 e01, e03, p;
	subtract_vector(rec->vertices[1], rec->vertices[0], e01);
	subtract_vector(rec->vertices[3], rec->vertices[0], e03);

	cross_product(ray_d, e03, p);

	double det = dot_product(e01, p);

	/* Reject rays orthagonal to the normal vector.
	* I.e. rays parallell to the plane.
	*/
	if (det < 1e-4)
		return 0;

	double inv_det = 1.0 / det;

	point3 s;
	subtract_vector(ray_e, rec->vertices[0], s);

	double alpha = inv_det * dot_product(s, p);

	if ((alpha > 1.0) || (alpha < 0.0))
		return 0;

	point3 q;
	cross_product(s, e01, q);

	double beta = inv_det * dot_product(ray_d, q);
	if ((beta > 1.0) || (beta < 0.0))
		return 0;

	*t1 = inv_det * dot_product(e03, q);

	if (alpha + beta > 1.0f) {
		/* for the second triangle */
		point3 e23, e21;
		subtract_vector(rec->vertices[3], rec->vertices[2], e23);
		subtract_vector(rec->vertices[1], rec->vertices[2], e21);

		cross_product(ray_d, e21, p);

		det = dot_product(e23, p);

		if (det < 1e-4)
			return 0;

		inv_det = 1.0 / det;
		subtract_vector(ray_e, rec->vertices[2], s);

		alpha = inv_det * dot_product(s, p);
		if (alpha < 0.0)
			return 0;

		cross_product(s, e23, q);
		beta = inv_det * dot_product(ray_d, q);

		if ((beta < 0.0) || (beta + alpha > 1.0))
			return 0;

		*t1 = inv_det * dot_product(e21, q);
	}

	if (*t1 < 1e-4)
		return 0;

	COPY_POINT3(ip->normal, rec->normal);
	if (dot_product(ip->normal, ray_d)>0.0)
		multiply_vector(ip->normal, -1, ip->normal);
	multiply_vector(ray_d, *t1, ip->point);
	add_vector(ray_e, ip->point, ip->point);
	return 1;
}

__device__ void localColor(color local_color,
	const color light_color, double diffuse,
	double specular, const object_fill *fill)
{
	color ambi = { 0.1, 0.1, 0.1 };
	color diff, spec, lightCo, surface;

	/* Local Color = ambient * surface +
	*               light * ( kd * surface * diffuse + ks * specular)
	*/

	COPY_COLOR(diff, fill->fill_color);
	multiply_vector(diff, fill->Kd, diff);
	multiply_vector(diff, diffuse, diff);
	COPY_COLOR(lightCo, light_color);
	multiply_vectors(diff, lightCo, diff);

	COPY_COLOR(spec, light_color);
	multiply_vector(spec, fill->Ks, spec);
	multiply_vector(spec, specular, spec);

	COPY_COLOR(surface, fill->fill_color);
	multiply_vectors(ambi, surface, ambi);
	add_vector(diff, ambi, diff);
	add_vector(diff, spec, diff);
	add_vector(local_color, diff, local_color);
}

__device__ void compute_specular_diffuse(double *diffuse,
	double *specular,
	const point3 d, const point3 l,
	const point3 n, double phong_pow)
{
	point3 d_copy, l_copy, middle, r;

	/* Calculate vector to eye V */
	COPY_POINT3(d_copy, d);
	multiply_vector(d_copy, -1, d_copy);
	normalize(d_copy);

	/* Calculate vector to light L */
	COPY_POINT3(l_copy, l);
	multiply_vector(l_copy, -1, l_copy);
	normalize(l_copy);

	/* Calculate reflection direction R */
	double tmp = dot_product(n, l_copy);
	multiply_vector(n, tmp, middle);
	multiply_vector(middle, 2, middle);
	subtract_vector(middle, l_copy, r);
	normalize(r);

	/* diffuse = max(0, dot_product(n, -l)) */
	*diffuse = MAX(0, dot_product(n, l_copy));

	/* specular = (dot_product(r, -d))^p */
	*specular = pow(MAX(0, dot_product(r, d_copy)), phong_pow);
}

__device__ void reflection(point3 r, const point3 d, const point3 n)
{
	/* r = d - 2(d . n)n */
	multiply_vector(n, -2.0 * dot_product(d, n), r);
	add_vector(r, d, r);
}

/* reference: https://www.opengl.org/sdk/docs/man/html/refract.xhtml */
__device__ void refraction(point3 t, const point3 I, const point3 N,
	double n1, double n2)
{
	double eta = n1 / n2;
	double dot_NI = dot_product(N, I);
	double k = 1.0 - eta * eta * (1.0 - dot_NI * dot_NI);
	if (k < 0.0 || n2 <= 0.0)
		t[0] = t[1] = t[2] = 0.0;
	else {
		point3 tmp;
		multiply_vector(I, eta, t);
		multiply_vector(N, eta * dot_NI + sqrt(k), tmp);
		subtract_vector(t, tmp, t);
	}
}

__device__ double fresnel(const point3 r, const point3 l,
	const point3 normal, double n1, double n2)
{
	/* TIR */
	if (length(l) < 0.99)
		return 1.0;
	double cos_theta_i = -dot_product(r, normal);
	double cos_theta_t = -dot_product(l, normal);
	double r_vertical_root = (n1 * cos_theta_i - n2 * cos_theta_t) /
		(n1 * cos_theta_i + n2 * cos_theta_t);
	double r_parallel_root = (n2 * cos_theta_i - n1 * cos_theta_t) /
		(n2 * cos_theta_i + n1 * cos_theta_t);
	return (r_vertical_root * r_vertical_root +
		r_parallel_root * r_parallel_root) / 2.0;
}


__device__ intersection ray_hit_object(const point3 e, const point3 d,
	double t0, double t1,
	int *hit_rectangular,
	//rectangular **hit_rectangular,
	int *hit_sphere, int sizeRectangles, int sizeSpheres)
{
	*hit_rectangular = -1;
	*hit_sphere = -1;
	/* set these to not hit */
	point3 biased_e;
	multiply_vector(d, t0, biased_e);
	add_vector(biased_e, e, biased_e);

	double nearest = t1;
	intersection result, tmpresult;


	for(int i =0; i<sizeRectangles;i++){
		if (rayRectangularIntersection(biased_e, d, &cst_rectangulars[i],
			&tmpresult, &t1) && (t1 < nearest)) {
			/* hit is closest so far */
			*hit_rectangular = i;
			nearest = t1;
			result = tmpresult;
		}
	}

	/* check the spheres */

	for (int i = 0; i<sizeSpheres; i++) {
		if (raySphereIntersection(biased_e, d, &cst_spheres[i],
			&tmpresult, &t1) && (t1 < nearest)) {
			*hit_sphere = i;
			*hit_rectangular = -1;
			nearest = t1;
			result = tmpresult;
		}
	}
	return result;
}
__device__ int ray_color(const point3 e, double t,
	const point3 d,
	idx_stack *stk,
	color object_color, int bounces_left, int sizeRectangles, int sizeSpheres, int sizeLights, int px)
{
	int hit_rect = -1;
	int light_hit_rec = -1;
	int hit_sphere = -1;
	int light_hit_sphere = -1;
	object_fill fill;
	double diffuse, specular;
	point3 l, _l, r, rr;	
	color reflection_part;
	color refraction_part;
	// might be a reflection ray, so check how many times we've bounced 
	if (bounces_left == 0) {
		SET_COLOR(object_color, 0.0, 0.0, 0.0);
		return 0;
	}

	/* check for intersection with a sphere or a rectangular */
	intersection ip = ray_hit_object(e, d, t, MAX_DISTANCE,
		&hit_rect, &hit_sphere, sizeRectangles, sizeSpheres);

	
	if (hit_rect == -1 && hit_sphere == -1){
		return 0;
	}

	void *hit_obj = NULL;
	if (hit_rect != -1) {
		fill = cst_rectangulars[hit_rect].rectangular_fill;
		hit_obj = &cst_rectangulars[hit_rect];
	}
	else {
		fill = cst_spheres[hit_sphere].sphere_fill;
		hit_obj = &cst_spheres[hit_sphere];
	}

	// assume it is a shadow 
	SET_COLOR(object_color, 0.0, 0.0, 0.0);
	

	for (int i = 0; i < sizeLights; i++ ){
		// calculate the intersection vector pointing at the light 
		subtract_vector(ip.point, cst_lights[i].position, l);
		multiply_vector(l, -1, _l);
		normalize(_l);
		/// check for intersection with an object. use ignore_me
		// because we don't care about this normal

		ray_hit_object(ip.point, _l, MIN_DISTANCE, length(l),
			&light_hit_rec,
			&light_hit_sphere, sizeRectangles, sizeSpheres);
		
		// the light was not block by itself(lit object) 
		//se il raggio che va dal punto alla luce colpisce qualche oggetto vai avanti (ombra?)
		if (light_hit_rec !=-1 || light_hit_sphere != -1)
			continue;
		//fill = hit_rec ?
		//	hit_rec->rectangular_fill :
		//	hit_sphere->sphere_fill;
		compute_specular_diffuse(&diffuse, &specular, d, l,
			ip.normal, fill.phong_power);

		localColor(object_color, cst_lights[i].light_color,
			diffuse, specular, &fill);
	}
	
	reflection(r, d, ip.normal);
	double idx = idx_stack_topcuda(stk).idx, idx_pass = fill.index_of_refraction;
	
	if (idx_stack_topcuda(stk).obj == hit_obj) {
		idx_stack_popcuda(stk);
		idx_pass = idx_stack_topcuda(stk).idx;
	}
	else {
		idx_stack_element e;
		e.obj = hit_obj;
		e.idx = fill.index_of_refraction;
		idx_stack_pushcuda(stk, e);
	}


	refraction(rr, d, ip.normal, idx, idx_pass);
	double R = (fill.T > 0.1) ?
		fresnel(d, rr, ip.normal, idx, idx_pass) :
		1.0;
	
	if (fill.R > 0) {
		// if we hit something, add the color 
		int old_top = stk->top;
		
		if (ray_color(ip.point, MIN_DISTANCE, r, stk, reflection_part,
			bounces_left - 1, sizeRectangles, sizeSpheres, sizeLights,px)) {
			multiply_vector(reflection_part, R * (1.0 - fill.Kd) * fill.R,
				reflection_part);
			add_vector(object_color, reflection_part,
				object_color);
		}
		stk->top = old_top;
	}

	// calculate refraction ray 
	if ((length(rr) > 0.0) && (fill.T > 0.0) &&
		(fill.index_of_refraction > 0.0)) {
		normalize(rr);
		if (ray_color(ip.point, MIN_DISTANCE, rr, stk, refraction_part,
			bounces_left - 1, sizeRectangles, sizeSpheres, sizeLights,px)) {
			multiply_vector(refraction_part, (1 - R) * fill.T,
				refraction_part);
			add_vector(object_color, refraction_part,
				object_color);
		}
	}

	protect_color_overflow(object_color);
	return 1;
}
/* @param background_color this is not ambient light */
__global__ void raytracingcuda(uint8_t *pixels, int sizeRectangles, int sizeLights, int sizeSpheres)
{
	//trovo la posizione y
	int iy = blockIdx.y * blockDim.y + threadIdx.y;
	//trovo la posizione x
	int ix = blockIdx.x * blockDim.x + threadIdx.x;

	if (iy < ROWS && ix < COLS) {
		int idx = iy * COLS + ix;
		//calculate basis vector si fa nell'host
		//uvwd in constant memory.. 
			point3 u, v, w, d;

			color object_color = { 0.0, 0.0, 0.0 };

			// calculate u, v, w 
			calculateBasisVectors(u, v, w, cst_view); 

			idx_stack stk;

			int factor = sqrtf(SAMPLES);
			double r = 0, g = 0, b = 0;

			// MSAA 
			for (int s = 0; s < SAMPLES; s++) {
				idx_stack_initcuda(&stk);

				rayConstruction(d, u, v, w,
					ix * factor + s / factor,
					iy * factor + s % factor,
					COLS * factor, ROWS * factor);
				
				if (ray_color(cst_view[0].vrp, 0.0, d, &stk, object_color, MAX_REFLECTION_BOUNCES, sizeRectangles, sizeSpheres, sizeLights, idx)) {
					r += object_color[0];
					g += object_color[1];
					b += object_color[2];
				}
				else {
					r += cst_backgroundColor[0];
					g += cst_backgroundColor[1];
					b += cst_backgroundColor[2];
				}
				pixels[idx * 3 + 0] = r * 255 / SAMPLES;
				pixels[idx * 3 + 1] = g * 255 / SAMPLES;
				pixels[idx * 3 + 2] = b * 255 / SAMPLES;

			}
			
	}
	//}
}

void callraytracingcuda(dim3 grid, dim3 block, long nBytesPixel, color backgroundcolor
			 , rectangular listRectangles[], sphere listSphere[], light listLight[], viewpoint view, int sizeRectangles, int sizeSpheres, int sizeLights, unsigned char *pixelscu) {

	size_t size_stack;
	CHECK(cudaDeviceSetLimit(cudaLimitStackSize, 3072)); //2048
	CHECK(cudaDeviceGetLimit(&size_stack, cudaLimitStackSize));
	cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);

	uint8_t *dev_pixels;
	
	cudaEvent_t start, stop;
	float time;
	CHECK(cudaEventCreate(&start));
	CHECK(cudaEventCreate(&stop));
	// record start event
	
	CHECK(cudaEventRecord(start));

	CHECK(cudaMalloc((uint8_t**)&dev_pixels, nBytesPixel));
	CHECK(cudaMemcpyToSymbol(cst_backgroundColor, backgroundcolor, sizeof(color)));
	CHECK(cudaMemcpyToSymbol(cst_rectangulars, listRectangles, sizeof(rectangular)* sizeRectangles));
	CHECK(cudaMemcpyToSymbol(cst_spheres, listSphere, sizeof(sphere)*sizeSpheres));
	CHECK(cudaMemcpyToSymbol(cst_lights, listLight, sizeof(light)* sizeLights));
	CHECK(cudaMemcpyToSymbol(cst_view, &view, sizeof(viewpoint)));
	raytracingcuda <<<grid, block >> >(dev_pixels, sizeRectangles, sizeLights, sizeSpheres);
	cudaError_t errSync = cudaGetLastError();
	CHECK(cudaDeviceSynchronize());
	CHECK(cudaMemcpy(pixelscu, dev_pixels, nBytesPixel, cudaMemcpyDeviceToHost));
	CHECK(cudaEventRecord(stop));
	// wait until the stop event completes
	CHECK(cudaEventSynchronize(stop));
	CHECK(cudaEventElapsedTime(&time, start, stop));

	printf("milliseconds cuda: %f \n", time);

	if (errSync != cudaSuccess)
	{
		printf("Sync kernel error: %s\n", cudaGetErrorString(errSync));
	}

	//free:
	CHECK(cudaFree(dev_pixels));
}

