#ifndef __RAYTRACINGCUDA_H
#define __RAYTRACINGCUDA_H

#include <stdint.h>
#include "common.h"

void callraytracingcuda(dim3 grid, dim3 block, long nBytesPixel, color backgroundcolor
	,rectangular listRectangles[], sphere listSphere[], light listLight[], viewpoint view, int sizeRectangles, int sizeSpheres, int sizeLights, unsigned char *pixelscu);

#endif
